﻿using NINA.Model.MyGuider;
using NINA.Model;
using NINA.Utility;
using NINA.Utility.Enum;
using NINA.Utility.Mediator;
using NINA.Utility.Profile;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace NINA.ViewModel {

    internal class GuiderVM : DockableVM {

        public GuiderVM(IProfileService profileService) : base(profileService) {
            Title = "LblGuider";
            ContentId = nameof(GuiderVM);
            ImageGeometry = (System.Windows.Media.GeometryGroup)System.Windows.Application.Current.Resources["GuiderSVG"];
            ConnectGuiderCommand = new AsyncCommand<bool>(
                async () =>
                    await Task.Run<bool>(() => Connect()),
                (object o) =>
                    !(Guider?.Connected == true)
            );
            DisconnectGuiderCommand = new RelayCommand((object o) => Disconnect(), (object o) => Guider?.Connected == true);

            /*SetUpPlotModels();*/

            MaxY = 4;
            MaxDurationY = 1;

            GuideStepsHistory = new AsyncObservableLimitedSizedStack<IGuideStep>(HistorySize);
            GuideStepsHistoryMinimal = new AsyncObservableLimitedSizedStack<IGuideStep>(MinimalHistorySize);
            RMS = new RMS();

            RegisterMediatorMessages();
        }

        public enum GuideStepsHistoryType {
            GuideStepsLarge,
            GuideStepsMinimal
        }

        private void RegisterMediatorMessages() {
            Mediator.Instance.RegisterAsyncRequest(
                new DitherGuiderMessageHandle(async (DitherGuiderMessage msg) => {
                    return await Dither(msg.Token);
                })
            );

            Mediator.Instance.RegisterRequest(
                new GuideStepHistoryCountMessageHandle((GuideStepHistoryCountMessage msg) => {
                    return ChangeGuideSteps(msg.GuideSteps, msg.HistoryType);
                })
            );

            Mediator.Instance.RegisterAsyncRequest(
                new PauseGuiderMessageHandle(async (PauseGuiderMessage msg) => {
                    if (msg.Pause) {
                        return await Pause(msg.Token);
                    } else {
                        return await Resume(msg.Token);
                    }
                })
            );

            Mediator.Instance.RegisterAsyncRequest(
                new AutoSelectGuideStarMessageHandle(async (AutoSelectGuideStarMessage msg) => {
                    return await AutoSelectGuideStar(msg.Token);
                })
            );

            Mediator.Instance.RegisterAsyncRequest(
                new StartGuiderMessageHandle(async (StartGuiderMessage msg) => {
                    return await StartGuiding(msg.Token);
                })
            );

            Mediator.Instance.RegisterAsyncRequest(
                new StopGuiderMessageHandle(async (StopGuiderMessage msg) => {
                    return await StopGuiding(msg.Token);
                })
            );
        }

        private async Task<bool> AutoSelectGuideStar(CancellationToken token) {
            if (Guider?.Connected == true) {
                var result = await Guider?.AutoSelectGuideStar();
                await Task.Delay(TimeSpan.FromSeconds(5), token);
                return result;
            } else {
                return false;
            }
        }

        private async Task<bool> Pause(CancellationToken token) {
            if (Guider?.Connected == true) {
                return await Guider?.Pause(true, token);
            } else {
                return false;
            }
        }

        private async Task<bool> Resume(CancellationToken token) {
            if (Guider?.Connected == true) {
                await Guider?.Pause(false, token);
                await Utility.Utility.Wait(TimeSpan.FromSeconds(profileService.ActiveProfile.GuiderSettings.SettleTime), token);
                return true;
            } else {
                return false;
            }
        }

        public int HistorySize {
            get {
                return profileService.ActiveProfile.GuiderSettings.PHD2HistorySize;
            }
            set {
                profileService.ActiveProfile.GuiderSettings.PHD2HistorySize = value;
                RaisePropertyChanged();
            }
        }

        public int MinimalHistorySize {
            get {
                return profileService.ActiveProfile.GuiderSettings.PHD2HistorySize;
            }
            set {
                profileService.ActiveProfile.GuiderSettings.PHD2MinimalHistorySize = value;
                RaisePropertyChanged();
            }
        }

        private static Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;

        private bool ChangeGuideSteps(int historySize, GuideStepsHistoryType historyType) {
            AsyncObservableLimitedSizedStack<IGuideStep> collectionToChange = new AsyncObservableLimitedSizedStack<IGuideStep>(0);

            switch (historyType) {
                case GuideStepsHistoryType.GuideStepsLarge:
                    collectionToChange = GuideStepsHistory;
                    break;

                case GuideStepsHistoryType.GuideStepsMinimal:
                    collectionToChange = GuideStepsHistoryMinimal;
                    break;
            }

            collectionToChange.MaxSize = historySize;

            return true;
        }

        private void ResetGraphValues() {
            GuideStepsHistory.Clear();
            GuideStepsHistoryMinimal.Clear();
            RMS.Clear();
            MaxDurationY = 1;
        }

        private async Task<bool> Connect() {
            ResetGraphValues();
            Guider = new PHD2Guider(profileService);
            Guider.PropertyChanged += Guider_PropertyChanged;
            return await Guider.Connect();
        }

        private bool Disconnect() {
            ResetGraphValues();
            var discon = Guider.Disconnect();
            Guider = null;
            return discon;
        }

        private void Guider_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "PixelScale") {
                PixelScale = Guider.PixelScale;
            }
            if (e.PropertyName == "GuideStep") {
                var step = Guider.GuideStep;
                if (GuiderScale == GuiderScaleEnum.ARCSECONDS) {
                    ConvertStepToArcSec(step);
                }
                GuideStepsHistoryMinimal.Add(step);
                GuideStepsHistory.Add(step);
                RMS.AddDataPoint(step.RADistanceRaw, step.DecDistanceRaw);

                if(Math.Abs(step.DECDuration) > MaxDurationY || Math.Abs(step.RADuration) > MaxDurationY) {
                    MaxDurationY = Math.Max(Math.Abs(step.RADuration), Math.Abs(step.DECDuration));
                }
            }            
        }       

        private RMS rms;
        public RMS RMS {
            get {
                return rms;
            }
            set {
                rms = value;
                RaisePropertyChanged();
            }
        }        

        private void ConvertStepToArcSec(IGuideStep pixelStep) {
            // only displayed values are changed, not the raw ones
            pixelStep.RADistanceRawDisplay = pixelStep.RADistanceRaw * PixelScale;
            pixelStep.DecDistanceRawDisplay = pixelStep.DecDistanceRaw * PixelScale;
            pixelStep.RADistanceGuideDisplay = pixelStep.RADistanceGuide * PixelScale;
            pixelStep.DecDistanceGuideDisplay = pixelStep.DecDistanceGuide * PixelScale;
        }

        private void ConvertStepToPixels(IGuideStep arcsecStep) {
            arcsecStep.RADistanceRawDisplay = arcsecStep.RADistanceRaw / PixelScale;
            arcsecStep.DecDistanceRawDisplay = arcsecStep.DecDistanceRaw / PixelScale;
            arcsecStep.RADistanceGuideDisplay = arcsecStep.RADistanceGuide / PixelScale;
            arcsecStep.DecDistanceGuideDisplay = arcsecStep.DecDistanceGuide / PixelScale;
        }

        public GuiderScaleEnum GuiderScale {
            get {
                return profileService.ActiveProfile.GuiderSettings.PHD2GuiderScale;
            }
            set {
                profileService.ActiveProfile.GuiderSettings.PHD2GuiderScale = value;
                RaisePropertyChanged();
                foreach (IGuideStep s in GuideStepsHistory) {
                    if (GuiderScale == GuiderScaleEnum.ARCSECONDS) {
                        ConvertStepToArcSec(s);
                    } else {
                        ConvertStepToPixels(s);
                    }
                }
                foreach (IGuideStep s in GuideStepsHistoryMinimal) {
                    if (GuiderScale == GuiderScaleEnum.ARCSECONDS) {
                        ConvertStepToArcSec(s);
                    } else {
                        ConvertStepToPixels(s);
                    }
                }

                RMS.SetScale(GuiderScale == GuiderScaleEnum.ARCSECONDS ? PixelScale : 1);

                RaisePropertyChanged(nameof(GuideStepsHistory));
                RaisePropertyChanged(nameof(GuideStepsHistoryMinimal));
            }
        }

        private double _pixelScale;

        public double PixelScale {
            get {
                return _pixelScale;
            }
            set {
                _pixelScale = value;
                RaisePropertyChanged();
            }
        }

        private async Task<bool> StartGuiding(CancellationToken token) {
            if (Guider?.Connected == true) {
                return await Guider.StartGuiding(token);
            } else {
                return false;
            }
        }

        private async Task<bool> StopGuiding(CancellationToken token) {
            if (Guider?.Connected == true) {
                return await Guider.StopGuiding(token);
            } else {
                return false;
            }
        }

        private async Task<bool> Dither(CancellationToken token) {
            if (Guider?.Connected == true) {
                Mediator.Instance.Request(new StatusUpdateMessage() { Status = new Model.ApplicationStatus() { Status = Locale.Loc.Instance["LblDither"], Source = Title } });
                await Guider?.Dither(token);
                Mediator.Instance.Request(new StatusUpdateMessage() { Status = new Model.ApplicationStatus() { Status = string.Empty, Source = Title } });
                return true;
            } else {
                return false;
            }
        }

        public AsyncObservableLimitedSizedStack<IGuideStep> GuideStepsHistory { get; set; }
        public AsyncObservableLimitedSizedStack<IGuideStep> GuideStepsHistoryMinimal { get; set; }

        private IGuider _guider;

        public IGuider Guider {
            get {
                return _guider;
            }
            set {
                _guider = value;
                RaisePropertyChanged();
            }
        }

        public double Interval {
            get {
                return MaxY / 4;
            }
        }

        private double _maxY;

        public double MaxY {
            get {
                return _maxY;
            }

            set {
                _maxY = value;
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(MinY));
                RaisePropertyChanged(nameof(Interval));
            }
        }

        public double MinY {
            get {
                return -MaxY;
            }
        }

        private double _maxDurationY;

        public double MaxDurationY {
            get {
                return _maxDurationY;
            }

            set {
                _maxDurationY = value;
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(MinDurationY));
            }
        }

        public double MinDurationY {
            get {
                return -MaxDurationY;
            }
        }

        public ICommand ConnectGuiderCommand { get; private set; }

        public ICommand DisconnectGuiderCommand { get; private set; }
    }
}
﻿using NINA.Model;
using NINA.Utility.Mediator;
using NINA.Utility.Profile;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace NINA.ViewModel {

    internal class ApplicationStatusVM : DockableVM {

        public ApplicationStatusVM(IProfileService profileService) : base(profileService) {
            Title = "LblApplicationStatus";
            ContentId = nameof(ApplicationStatusVM);
            ImageGeometry = (System.Windows.Media.GeometryGroup)System.Windows.Application.Current.Resources["ApplicationStatusSVG"];
            RegisterMediatorMessages();
        }

        private string _status;

        public string Status {
            get {
                return _status;
            }
            private set {
                _status = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<ApplicationStatus> _applicationStatus = new ObservableCollection<ApplicationStatus>();

        public ObservableCollection<ApplicationStatus> ApplicationStatus {
            get {
                return _applicationStatus;
            }
            set {
                _applicationStatus = value;
                RaisePropertyChanged();
            }
        }

        private static Dispatcher _dispatcher = Application.Current?.Dispatcher ?? Dispatcher.CurrentDispatcher;

        private void RegisterMediatorMessages() {
            Mediator.Instance.RegisterRequest(
                new StatusUpdateMessageHandle((StatusUpdateMessage msg) => {
                    _dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                        var status = msg.Status;
                        var item = ApplicationStatus.Where((x) => x.Source == status.Source).FirstOrDefault();
                        if (item != null) {
                            if (!string.IsNullOrEmpty(status.Status)) {
                                item.Status = status.Status;
                                item.Progress = status.Progress;
                                item.MaxProgress = status.MaxProgress;
                                item.ProgressType = status.ProgressType;
                                item.Status2 = status.Status2;
                                item.Progress2 = status.Progress2;
                                item.MaxProgress2 = status.MaxProgress2;
                                item.ProgressType2 = status.ProgressType2;
                                item.Status3 = status.Status3;
                                item.Progress3 = status.Progress3;
                                item.MaxProgress3 = status.MaxProgress3;
                                item.ProgressType3 = status.ProgressType3;
                            } else {
                                ApplicationStatus.Remove(item);
                            }
                        } else {
                            if (!string.IsNullOrEmpty(status.Status)) {
                                ApplicationStatus.Add(new ApplicationStatus() {
                                    Source = status.Source,
                                    Status = status.Status,
                                    Progress = status.Progress,
                                    MaxProgress = status.MaxProgress,
                                    ProgressType = status.ProgressType,
                                    Status2 = status.Status2,
                                    Progress2 = status.Progress2,
                                    MaxProgress2 = status.MaxProgress2,
                                    ProgressType2 = status.ProgressType2,                                    
                                    Status3 = status.Status3,
                                    Progress3 = status.Progress3,
                                    MaxProgress3 = status.MaxProgress3,
                                    ProgressType3 = status.ProgressType3
                                });
                            }
                        }

                        RaisePropertyChanged(nameof(ApplicationStatus));
                    }));
                    return true;
                })
            );
        }
    }
}
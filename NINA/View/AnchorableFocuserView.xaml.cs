﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableFocuserView.xaml
    /// </summary>
    public partial class AnchorableFocuserView : UserControl {

        public AnchorableFocuserView() {
            InitializeComponent();
        }
    }
}
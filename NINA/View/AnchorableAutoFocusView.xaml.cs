﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableAutoFocusView.xaml
    /// </summary>
    public partial class AnchorableAutoFocusView : UserControl {

        public AnchorableAutoFocusView() {
            InitializeComponent();
        }
    }
}
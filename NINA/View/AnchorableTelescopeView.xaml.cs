﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableTelescopeView.xaml
    /// </summary>
    public partial class AnchorableTelescopeView : UserControl {

        public AnchorableTelescopeView() {
            InitializeComponent();
        }
    }
}
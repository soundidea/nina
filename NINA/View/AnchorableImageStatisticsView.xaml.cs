﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableImageStatisticsView.xaml
    /// </summary>
    public partial class AnchorableImageStatisticsView : UserControl {

        public AnchorableImageStatisticsView() {
            InitializeComponent();
        }
    }
}
﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableGuiderView.xaml
    /// </summary>
    public partial class AnchorableGuiderView : UserControl {

        public AnchorableGuiderView() {
            InitializeComponent();
        }
    }
}
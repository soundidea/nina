﻿<Window
    x:Class="NINA.MainWindow"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
    xmlns:local="clr-namespace:NINA"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:util="clr-namespace:NINA.Utility"
    xmlns:view="clr-namespace:NINA.View"
    Name="ThisWindow"
    Title="N.I.N.A. - Nighttime Imaging 'N' Astronomy"
    MinWidth="1280"
    MinHeight="720"
    Background="{StaticResource BackgroundBrush}"
    DataContext="{StaticResource AppVM}"
    Icon="/NINA;component/Resources/observatory.png"
    Style="{StaticResource MainWindow}">

    <Window.InputBindings>
        <KeyBinding Key="F5" Command="{Binding Source={StaticResource AppVM}, Path=ConnectAllDevicesCommand}" />
        <KeyBinding Key="F9" Command="{Binding Source={StaticResource AppVM}, Path=DisconnectAllDevicesCommand}" />
    </Window.InputBindings>
    <i:Interaction.Triggers>
        <i:EventTrigger EventName="Loaded">
            <i:InvokeCommandAction Command="{Binding CheckProfileCommand}" />
            <i:InvokeCommandAction Command="{Binding CheckUpdateCommand}" />
        </i:EventTrigger>
    </i:Interaction.Triggers>
    <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition />
                <RowDefinition Height="30" />
            </Grid.RowDefinitions>
            <Grid>
                <TabControl SelectedIndex="{Binding TabIndex}" TabStripPlacement="Left">
                    <TabItem>
                        <TabItem.Header>
                            <Grid>
                                <Grid Width="50" Height="50">
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource CameraSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}" />
                                </Grid>

                                <Grid Visibility="{Binding CameraVM.Cam, Converter={StaticResource NullToVisibilityConverter}}">
                                    <Path
                                        Width="10"
                                        Height="10"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Bottom"
                                        Data="{StaticResource PowerSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}"
                                        Visibility="{Binding CameraVM.Cam.Connected, Converter={StaticResource VisibilityConverter}}" />
                                </Grid>
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:CameraView DataContext="{Binding CameraVM}" />
                        </TabItem.Content>
                    </TabItem>
                    <TabItem>
                        <TabItem.Header>
                            <UniformGrid Height="50" Columns="2">
                                <Grid>
                                    <Grid Width="25" Height="25">
                                        <Path
                                            Margin="2.5"
                                            Data="{StaticResource FWSVG}"
                                            Stretch="Uniform"
                                            Style="{StaticResource TabItemPath}" />
                                    </Grid>

                                    <Grid Margin="0,0,0,5" Visibility="{Binding FilterWheelVM.FW, Converter={StaticResource NullToVisibilityConverter}}">
                                        <Path
                                            Width="10"
                                            Height="10"
                                            HorizontalAlignment="Right"
                                            VerticalAlignment="Bottom"
                                            Data="{StaticResource PowerSVG}"
                                            Stretch="Uniform"
                                            Style="{StaticResource TabItemPath}"
                                            Visibility="{Binding FilterWheelVM.FW.Connected, Converter={StaticResource VisibilityConverter}}" />
                                    </Grid>
                                </Grid>
                                <Grid>
                                    <Grid Width="25" Height="25">
                                        <Path
                                            Margin="2.5"
                                            Data="{StaticResource FocusSVG}"
                                            Stretch="Uniform"
                                            Style="{StaticResource TabItemPath}" />
                                    </Grid>

                                    <Grid Margin="0,0,0,5" Visibility="{Binding FocuserVM.Focuser, Converter={StaticResource NullToVisibilityConverter}}">
                                        <Path
                                            Width="10"
                                            Height="10"
                                            HorizontalAlignment="Right"
                                            VerticalAlignment="Bottom"
                                            Data="{StaticResource PowerSVG}"
                                            Stretch="Uniform"
                                            Style="{StaticResource TabItemPath}"
                                            Visibility="{Binding FocuserVM.Focuser.Connected, Converter={StaticResource VisibilityConverter}}" />
                                    </Grid>
                                </Grid>
                            </UniformGrid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <UniformGrid Columns="2">
                                <view:FilterWheelView DataContext="{Binding FilterWheelVM}" />
                                <view:FocuserView DataContext="{Binding FocuserVM}" />
                            </UniformGrid>
                        </TabItem.Content>
                    </TabItem>
                    <TabItem>
                        <TabItem.Header>
                            <Grid>
                                <Grid Width="50" Height="50">
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource TelescopeSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}" />
                                </Grid>

                                <Grid Visibility="{Binding TelescopeVM.Telescope, Converter={StaticResource NullToVisibilityConverter}}">
                                    <Path
                                        Width="10"
                                        Height="10"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Bottom"
                                        Data="{StaticResource PowerSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}"
                                        Visibility="{Binding TelescopeVM.Telescope.Connected, Converter={StaticResource VisibilityConverter}}" />
                                </Grid>
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:TelescopeView DataContext="{Binding TelescopeVM}" />
                        </TabItem.Content>
                    </TabItem>
                    <TabItem>
                        <TabItem.Header>
                            <Grid>
                                <Grid Width="50" Height="50">
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource GuiderSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}" />
                                </Grid>

                                <Grid Visibility="{Binding GuiderVM.Guider, Converter={StaticResource NullToVisibilityConverter}}">
                                    <Path
                                        Width="10"
                                        Height="10"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Bottom"
                                        Data="{StaticResource PowerSVG}"
                                        Stretch="Uniform"
                                        Style="{StaticResource TabItemPath}"
                                        Visibility="{Binding GuiderVM.Guider.Connected, Converter={StaticResource VisibilityConverter}}" />
                                </Grid>
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:GuiderView DataContext="{Binding GuiderVM}" />
                        </TabItem.Content>
                    </TabItem>

                    <TabItem>
                        <TabItem.Header>
                            <Grid Width="50" Height="50">
                                <Path
                                    Margin="5"
                                    Data="{StaticResource MapSVG}"
                                    Stretch="Uniform"
                                    Style="{StaticResource TabItemPath}" />
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:SkyAtlasView DataContext="{Binding SkyAtlasVM}" />
                        </TabItem.Content>
                    </TabItem>

                    <TabItem>
                        <TabItem.Header>
                            <Grid Width="50" Height="50">
                                <Path
                                    Margin="5"
                                    Data="{StaticResource FocusAssistantSVG}"
                                    Stretch="Uniform"
                                    Style="{StaticResource TabItemPath}" />
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:FramingAssistantView DataContext="{Binding FramingAssistantVM}" />
                        </TabItem.Content>
                    </TabItem>

                    <TabItem>
                        <TabItem.Header>
                            <Grid Width="50" Height="50">
                                <Path
                                    Margin="5"
                                    Data="{StaticResource SequenceSVG}"
                                    Stretch="Uniform"
                                    Style="{StaticResource TabItemPath}" />
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:SequenceView DataContext="{Binding SeqVM}" />
                        </TabItem.Content>
                    </TabItem>

                    <TabItem>
                        <TabItem.Header>
                            <Grid Width="50" Height="50">
                                <Path
                                    Margin="5"
                                    Data="{StaticResource PictureSVG}"
                                    Stretch="Uniform"
                                    Style="{StaticResource TabItemPath}" />
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:OverView x:Name="Overview" DataContext="{Binding DockManagerVM}" />
                        </TabItem.Content>
                    </TabItem>
                    <TabItem>
                        <TabItem.Header>
                            <Grid Width="50" Height="50">
                                <Path
                                    Margin="5"
                                    Data="{StaticResource SettingsSVG}"
                                    Stretch="Uniform"
                                    Style="{StaticResource TabItemPath}" />
                            </Grid>
                        </TabItem.Header>
                        <TabItem.Content>
                            <view:OptionsView DataContext="{Binding OptionsVM}" />
                        </TabItem.Content>
                    </TabItem>
                </TabControl>
                <Grid
                    Width="80"
                    HorizontalAlignment="Left"
                    VerticalAlignment="Bottom">
                    <ninactrl:CancellableButton
                        Width="40"
                        Height="40"
                        Margin="10"
                        VerticalAlignment="Center"
                        Background="{StaticResource BackgroundBrush}"
                        ButtonForegroundBrush="{StaticResource PrimaryBrush}"
                        ButtonImage="{StaticResource PowerSVG}"
                        ButtonStyle="{StaticResource BackgroundButton}"
                        Command="{Binding ConnectAllDevicesCommand}"
                        ToolTip="{ns:Loc LblConnect}" />
                    <ninactrl:LoadingControl
                        Width="40"
                        Height="40"
                        Margin="5"
                        LoadingImageBrush="{StaticResource PrimaryBrush}"
                        Visibility="{Binding ConnectAllDevicesCommand.Execution.IsNotCompleted, Converter={StaticResource BooleanToVisibilityCollapsedConverter}, FallbackValue=Collapsed}" />
                </Grid>
            </Grid>
            <Border
                Grid.Row="1"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0,1,0,0">

                <ItemsControl VerticalAlignment="Center" ItemsSource="{Binding Path=ApplicationStatusVM.ApplicationStatus}">
                    <ItemsControl.ItemsPanel>
                        <ItemsPanelTemplate>
                            <StackPanel Orientation="Horizontal" />
                        </ItemsPanelTemplate>
                    </ItemsControl.ItemsPanel>
                    <ItemsControl.ItemTemplate>
                        <DataTemplate>
                            <Border
                                Margin="5,0,5,0"
                                BorderBrush="{StaticResource BorderBrush}"
                                BorderThickness="0,0,1,0">
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock VerticalAlignment="Center" Text="{Binding Source}" />
                                    <TextBlock
                                        Margin="5,0,5,0"
                                        VerticalAlignment="Center"
                                        Text=":" />
                                    <TextBlock
                                        Margin="5,0,5,0"
                                        VerticalAlignment="Center"
                                        Text="{Binding Status}" />
                                    <Grid
                                        Height="25"
                                        MinWidth="100"
                                        Margin="5,0,5,0">
                                        <ProgressBar
                                            x:Name="pbProgress"
                                            Height="20"
                                            VerticalAlignment="Center"
                                            Maximum="{Binding MaxProgress}"
                                            Minimum="0"
                                            Value="{Binding Progress}" />
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress, Path=Value, StringFormat=\{0:#%\}}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType}" Value="1">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress, Path=Value, StringFormat=\{0:#\}}" />
                                            <TextBlock VerticalAlignment="Center" Text=" / " />
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress, Path=Maximum}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType}" Value="0">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <Grid.Style>
                                            <Style TargetType="Grid">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding Progress}" Value="-1">
                                                        <Setter Property="Visibility" Value="Collapsed" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </Grid.Style>
                                    </Grid>
                                    <TextBlock
                                        Margin="5,0,5,0"
                                        VerticalAlignment="Center"
                                        Text="{Binding Status2}" />
                                    <Grid
                                        Height="25"
                                        MinWidth="100"
                                        Margin="5,0,5,0">
                                        <ProgressBar
                                            x:Name="pbProgress2"
                                            Height="20"
                                            VerticalAlignment="Center"
                                            Maximum="{Binding MaxProgress2}"
                                            Minimum="0"
                                            Value="{Binding Progress2}" />
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress2, Path=Value, StringFormat=\{0:#%\}}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType2}" Value="1">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress2, Path=Value, StringFormat=\{0:#\}}" />
                                            <TextBlock VerticalAlignment="Center" Text=" / " />
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress2, Path=Maximum}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType2}" Value="0">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <Grid.Style>
                                            <Style TargetType="Grid">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding Progress2}" Value="-1">
                                                        <Setter Property="Visibility" Value="Collapsed" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </Grid.Style>
                                    </Grid>
                                    <TextBlock
                                        Margin="5,0,5,0"
                                        VerticalAlignment="Center"
                                        Text="{Binding Status3}" />
                                    <Grid
                                        Height="25"
                                        MinWidth="100"
                                        Margin="5,0,5,0">
                                        <ProgressBar
                                            x:Name="pbProgress3"
                                            Height="20"
                                            VerticalAlignment="Center"
                                            Maximum="{Binding MaxProgress3}"
                                            Minimum="0"
                                            Value="{Binding Progress3}" />
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress3, Path=Value, StringFormat=\{0:#%\}}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType3}" Value="1">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <StackPanel
                                            HorizontalAlignment="Center"
                                            VerticalAlignment="Center"
                                            Orientation="Horizontal">
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress3, Path=Value, StringFormat=\{0:#\}}" />
                                            <TextBlock VerticalAlignment="Center" Text=" / " />
                                            <TextBlock VerticalAlignment="Center" Text="{Binding ElementName=pbProgress3, Path=Maximum}" />
                                            <StackPanel.Style>
                                                <Style TargetType="StackPanel">
                                                    <Style.Triggers>
                                                        <DataTrigger Binding="{Binding ProgressType3}" Value="0">
                                                            <Setter Property="Visibility" Value="Collapsed" />
                                                        </DataTrigger>
                                                    </Style.Triggers>
                                                </Style>
                                            </StackPanel.Style>
                                        </StackPanel>
                                        <Grid.Style>
                                            <Style TargetType="Grid">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding Progress3}" Value="-1">
                                                        <Setter Property="Visibility" Value="Collapsed" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </Grid.Style>
                                    </Grid>
                                </StackPanel>
                            </Border>
                        </DataTemplate>
                    </ItemsControl.ItemTemplate>
                </ItemsControl>
            </Border>
        </Grid>
    </Border>
</Window>